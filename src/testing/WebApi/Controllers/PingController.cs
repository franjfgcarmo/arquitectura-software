using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApi.Controllers
{
    [Route("api/[Controller]")]
    public class PingController
        : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public PingController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        [HttpGet]
        public IActionResult GetPing()
        {
            var currentTime = DateTime.UtcNow;
            var foo = _configuration.GetValue<string>("Foo");
            return Ok($"{currentTime} - Foo: {foo}");
        }
    }
}