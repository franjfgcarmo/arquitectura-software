using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using WebApi.FunctionalTests.TestSupport;
using Xunit;

namespace WebApi.FunctionalTests.UseCases.PingTests
{
    public static class GetPingTests
    {
        public class Given_An_Http_Endpoint_When_Getting_Ping
            : FunctionalTest
        {
            private string _url;
            private HttpResponseMessage _result;

            protected override Task Given()
            {
                _url = "api/ping";
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
            
            [Fact]
            public async Task Then_It_Should_Contain_Foo_Value_From_Test()
            {
                var responseJson = await _result.Content.ReadAsStringAsync();
                responseJson.Should().Contain("hola desde mis tests");
            }
        }
    }
}