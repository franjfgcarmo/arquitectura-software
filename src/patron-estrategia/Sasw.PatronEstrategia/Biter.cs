using System;

namespace Sasw.PatronEstrategia
{
    public class Biter
        : IFightingBehavior
    {
        public void Fight()
        {
            Console.WriteLine("Toma mordisco!");
        }
    }
}