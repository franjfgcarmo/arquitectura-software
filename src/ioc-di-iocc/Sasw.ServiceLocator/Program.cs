﻿namespace Sasw.ServiceLocator
{
    class Program
    {
        static void Main(string[] args)
        {
            //Register somewhere
            ServiceLocator.Register(typeof(ILogger), typeof(Logger));
            ServiceLocator.Register(typeof(IHardWorking), typeof(MyComponent));
            
            //Use it. No explicit instantiation!
            var hardWorking = ServiceLocator.GetService<IHardWorking>();
            hardWorking.DoStuff();
        }
    }

    public interface IHardWorking
    {
        void DoStuff();
    }

    public class MyComponent
        : IHardWorking
    {
        public void DoStuff()
        {
            var logger = ServiceLocator.GetService<ILogger>();
            logger.Log("I depend on a common service locator, so.. not great");
        }
    }
}