using System;
using System.Collections.Generic;

namespace Sasw.ServiceLocator
{
    public static class ServiceLocator
    {
        private static readonly IDictionary<Type, Type> Services = new Dictionary<Type, Type>();
        
        public static void Register(Type abstraction, Type implementation)

        {
            Services[abstraction] = implementation;
        } 
        
        public static T GetService<T>()
        {
            try
            {
                var serviceType = Services[typeof(T)];
                return (T) Activator.CreateInstance(serviceType);
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException("The requested service is not registered");
            }
        }
    }
}