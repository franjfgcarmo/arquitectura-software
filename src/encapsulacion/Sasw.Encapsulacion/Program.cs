﻿using System;

namespace Sasw.Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            var adult = new Person("Joe", 36);
            adult.Drink(Drink.Whiskey);
            Console.WriteLine(adult);
            
            // CAN'T DO DANGEROUS THINGS
            //adult.Name = null;
            //adult.Age = -1;
            //adult.PhysicalState = PhysicalState.Thirsty;
        }
    }
}