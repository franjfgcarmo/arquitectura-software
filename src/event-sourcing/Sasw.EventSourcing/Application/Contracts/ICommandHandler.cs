using System;

namespace Sasw.EventSourcing.Application.Contracts
{
    public interface ICommandHandler<in TCommand>
        where TCommand : class, ICommand
    {
        Guid Handle(TCommand command);
    }
}