using System.Collections.Generic;
using Sasw.EventSourcing.Domain.Contracts;

namespace Sasw.EventSourcing.Application.Contracts
{
    public interface IEventStore
    {
        IEnumerable<IDomainEvent> GetEvents(string streamName);
        void PersistEvents(string streamName, IEnumerable<IDomainEvent> domainEvents);
    }
}