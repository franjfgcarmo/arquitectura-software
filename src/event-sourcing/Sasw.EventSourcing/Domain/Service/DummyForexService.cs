using System;

namespace Sasw.EventSourcing.Domain.Service
{
    public class DummyForexService
        : IForexService
    {
        private const decimal DefaultExchangeRate = (decimal)1.1631;
        private decimal _exchangeRate = DefaultExchangeRate;

        public decimal GetCurrentUsdExchangeRate(string currencyCode)
        {
            if (currencyCode != "EUR")
            {
                throw new Exception($"{currencyCode} not supported");
            }

            return _exchangeRate;
        }

        public void SetEurExchangeRate(decimal exchangeRate)
        {
            _exchangeRate = exchangeRate;
        }
    }
}