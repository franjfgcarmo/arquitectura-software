﻿using System;

namespace Sasw.Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            var warrior = new Warrior();
            warrior.Name = "Valdivia";
            warrior.SayName();
            
            var person = new Person();
            person.Name = "Ines";
            person.SayName();
        }
    }

    class Person
    {
        public string Name { get; set; }

        public void SayName()
        {
            Console.WriteLine($"Soy una persona de tipo {this.GetType().Name} y me llamo {Name}");
        }
    }

    class Warrior
        : Person
    {
    }
}